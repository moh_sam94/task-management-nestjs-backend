## Overview

This project was implemented from [Udemy Nestjs Course](https://www.udemy.com/share/101XtiB0obcFdWR3Q=/)

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Database

- navigate to database directory
  `cd /database`
- Run docker compose file:
  `docker-compose up -d`
