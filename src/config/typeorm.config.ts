import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

const dbConfig = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: process.env.RDS_HOSTNAME || dbConfig.type,
  host: process.env.RDS_PORT || dbConfig.host,
  port: process.env.RDS_USERNAME || dbConfig.port,
  username: process.env.RDS_PASSWORD || dbConfig.username,
  password: process.env.RDS_DB_NAME || dbConfig.password,
  database: process.env.RDS_ || dbConfig.database,
  autoLoadEntities: true,
  synchronize: process.env.TYPEORM_SYNC || dbConfig.synchronize,
};
